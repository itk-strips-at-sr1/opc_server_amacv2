#!/bin/bash

python3 Cacophony/generateStuff.py \
    --dpt_prefix PP2AMAC --server_name PP2_AMAC \
    --subscription PP2_AMAC_SUBSCRIPTION \
    --driver_number 120 --function_prefix pp2AMAC

mv Cacophony/generated/configParser.ctl Cacophony/generated/configParser_PP2AMAC.ctl
mv Cacophony/generated/createDpts.ctl Cacophony/generated/createDpts_PP2AMAC.ctl
