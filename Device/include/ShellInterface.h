#ifndef __ShellInterface__H__
#define __ShellInterface__H__

#include "QuasarModuleITkStripsDCS.h"

#include <statuscode.h>

namespace Device
{

class ShellInterface
{
public:

    /*
    Run a shell command.
    Copied from DFelixServer::doShellCommand in the FELIX monitoring OPC server.
    */
    static OpcUa_StatusCode doShellCommand(const std::string &cmd, std::string &result);

private:

};

}

#endif // __ShellInterface__H__
