#include "ShellInterface.h"

#include <Configuration.hxx>
#include <LogIt.h>

#include <sstream>

namespace Device
{

OpcUa_StatusCode ShellInterface::doShellCommand(const std::string &cmd, std::string &result) {
	result="";
	std::array<char, 1024> buffer;

    // Open shell command for reading
	FILE* pipe = popen(cmd.c_str(), "r");
	if (!pipe) {
        // Log the popen error
		LOG(Log::ERR) << "Error while executing shell command: " << strerror(errno);
		return OpcUa_BadInternalError;
	}

    // Store output in the result
	while (!feof(pipe))
    {
		if (fgets(buffer.data(), buffer.size(), pipe) != nullptr) {
			result += buffer.data();
		}
        if (ferror(pipe)) {
            // Handle read error
            LOG(Log::ERR) << "Error while reading from shell command: " << strerror(errno);
            pclose(pipe);
            return OpcUa_BadInternalError;
        }
    }
    int status = pclose(pipe);
    if (status != 0){
        // Log exit status of shell command
        LOG(Log::ERR) << "Shell command returned non-zero exit status: " << status;
        return OpcUa_BadResourceUnavailable;
    }

	return OpcUa_Good;
}
}
