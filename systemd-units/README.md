# User mode of systemd

```
systemctl --user status  opc_amac
systemctl --user start   opc_amac
systemctl --user restart opc_amac
systemctl --user stop    opc_amac

journalctl -f --user-unit opc_amac
```

## Notice, systemd does not have `~/bin` in the user environment

Hence in the `export PATH` in the load script:

```
#!/bin/bash

source /home/itkstrips/setupFELIX_ALMA9.sh
export PATH=/home/itkstrips/bin:$PATH # we have the commandline influx client in ~/bin

/home/itkstrips/Documents/opc_server_amacv2/build/bin/OpcUaServer --config_file /home/itkstrips/.local/opt/atlas/etc/amac_opc_configs/swrod_flx03/config_BSS_d0-00-02-05-07-09-11_d1-02-03_noLpGBT_multithreaded.xml
```

When we move to telegraf, we can drop this dependency on the influx client.
