#!/bin/bash

source /home/itkstrips/setupFELIX_ALMA9.sh
export PATH=/home/itkstrips/bin:$PATH # we have the commandline influx client in ~/bin

/home/itkstrips/Documents/opc_server_amacv2/build/bin/OpcUaServer --config_file /home/itkstrips/.local/opt/atlas/etc/amac_opc_configs/swrod_flx03/config_BSS_d0-00-02-05-07-09-11_d1-02-03_noLpGBT_multithreaded.xml
