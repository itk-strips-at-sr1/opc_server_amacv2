#!/bin/bash
slow_delay=1 # for sleep command in the slow power up

if [ -z "$1" ]
then
  echo "provide the AMAC ID of the module" 1>&2
  exit 1
fi

>&2 echo "amac id $1"

default_cfg_file=/opt/atlas/etc/cfg_endeavour.json
default_daq_cfg_file=CERNSR1_stave_testing_setup.json

if [ -z "$2" ]
then
  cfg_file=$default_cfg_file
else
  cfg_file=$2
fi

>&2 echo "amac cfg $cfg_file"

if [ -z "$3" ]
then
  daq_cfg_file=$default_daq_cfg_file
else
  daq_cfg_file=$3
fi

>&2 echo "FE daq cfg $daq_cfg_file"

# setid argument
if [ -z "$4" ]
then
  echo no 4th argument - no SETID
else
  echo got a 4th argument $4 - sending SETID
  endeavour -c $cfg_file -i $1 setid idpads $1
fi


# star power up from write_amac_ppb_newer
echo regs

echo 42   0x00000000 ; endeavour -c $cfg_file -i $1 write  42   0x00000000  # no interlock or warnings enabled
echo 44   0x00000001 ; endeavour -c $cfg_file -i $1 write  44   0x00000001  # AM enabled 
echo 45   0x00000100 ; endeavour -c $cfg_file -i $1 write  45   0x00000100  # require DCDC PGOOD, but do not power from DCDC
echo 46   0x010d010a ; endeavour -c $cfg_file -i $1 write  46   0x010d010a  # BG tuning for one of amacs
echo 47   0x00000000 ; endeavour -c $cfg_file -i $1 write  47   0x00000000  # default
echo 48   0x00000000 ; endeavour -c $cfg_file -i $1 write  48   0x00000000  # shunt values at zero
echo 49   0x0000000D ; endeavour -c $cfg_file -i $1 write  49   0x0000000D  # DAC bias at default
echo 50   0x00000704 ; endeavour -c $cfg_file -i $1 write  50   0x00000704  # default AMACcnt, default is 0x404, max drive current 0x704
echo 51   0x03040404 ; endeavour -c $cfg_file -i $1 write  51   0x03040404  # default
echo 52   0x00401500 ; endeavour -c $cfg_file -i $1 write  52   0x00401500  # default

sleep $slow_delay
echo OFF
echo 41   0x00000000 ; endeavour -c $cfg_file -i $1 write  41   0x00000000  # DCDC disabled
echo 40   0x77770000 ; endeavour -c $cfg_file -i $1 write  40   0x77770000  # HV and LDOs are disabled -- low power mode
echo 43   0x00000000 ; endeavour -c $cfg_file -i $1 write  43   0x00000000  # HCC holding reset

sleep $slow_delay
echo DCDC
echo 41   0x00000001 ; endeavour -c $cfg_file -i $1 write  41   0x00000001  # DCDC enabled

sleep $slow_delay
echo HCCxLPM
echo 40   0x77770100 ; endeavour -c $cfg_file -i $1 write  40   0x77770100  # LDO LPMx

sleep $slow_delay
echo HCCxRESET
echo 43   0x00000100 ; endeavour -c $cfg_file -i $1 write  43   0x00000100  # HCC X reset

sleep $slow_delay
echo configure HCC x
bin/scanConsole -p -c $daq_cfg_file  -r ../configs/controller/felix_pcatlitkflx03_device0.json -l ../configs/logging/trace_star.json > /dev/null 2>&1 

sleep $slow_delay
echo ABCx0LPM
echo 40   0x77770300 ; endeavour -c $cfg_file -i $1 write  40   0x77770300  # LDO LPMx ABC 0

sleep $slow_delay
echo configure ABCx0
bin/scanConsole -p -c $daq_cfg_file  -r ../configs/controller/felix_pcatlitkflx03_device0.json -l ../configs/logging/trace_star.json > /dev/null 2>&1 

 
sleep $slow_delay
echo disable ABCx1 LPM
echo 40   0x77770700 ; endeavour -c $cfg_file -i $1 write  40   0x77770700  # LDO LPMx ABC 1

sleep $slow_delay
echo configure ABCx1
bin/scanConsole -p -c $daq_cfg_file  -r ../configs/controller/felix_pcatlitkflx03_device0.json -l ../configs/logging/trace_star.json > /dev/null 2>&1 

sleep $slow_delay
echo LPM Y

echo disable HCCy LPM
echo 40   0x77771700 ; endeavour -c $cfg_file -i $1 write  40   0x77771700  # LDO LPMy HCC

sleep $slow_delay
echo HCCyRESET
echo 43   0x00010100 ; endeavour -c $cfg_file -i $1 write  43   0x00010100  # HCC Y reset

sleep $slow_delay
echo configure HCCy
bin/scanConsole -p -c $daq_cfg_file  -r ../configs/controller/felix_pcatlitkflx03_device0.json -l ../configs/logging/trace_star.json > /dev/null 2>&1

sleep $slow_delay
echo disable ABCy0 LPM
echo 40   0x77773700 ; endeavour -c $cfg_file -i $1 write  40   0x77773700  # LDO LPMy ABC 0

sleep $slow_delay
echo configure ABCy0
bin/scanConsole -p -c $daq_cfg_file  -r ../configs/controller/felix_pcatlitkflx03_device0.json -l ../configs/logging/trace_star.json > /dev/null 2>&1 


sleep $slow_delay
echo disable ABCy1 LPM
echo 40   0x77777700 ; endeavour -c $cfg_file -i $1 write  40   0x77777700  # LDO LPMy ABC 1

sleep $slow_delay
echo configure ABCy1
bin/scanConsole -p -c $daq_cfg_file  -r ../configs/controller/felix_pcatlitkflx03_device0.json -l ../configs/logging/trace_star.json > /dev/null 2>&1 

