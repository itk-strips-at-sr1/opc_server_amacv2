#!/bin/python3

import logging
import argparse
import time


parser = argparse.ArgumentParser(
    formatter_class = argparse.RawDescriptionHelpFormatter,
    description = "generate a config file for the AMAC OPC server",
    epilog = """Example:
python generate_opc_configs.py opc
python generate_opc_configs.py -d --flx-hostname pcatlitkflx04 com

for i in `seq 0 7`
do
  ./generate_opc_configs.py --device 0 --link $i com
  ./generate_opc_configs.py --device 0 --link $i opc
  ./generate_opc_configs.py --device 0 --link $i lock
  ./generate_opc_configs.py --device 1 --link $i com
  ./generate_opc_configs.py --device 1 --link $i lock
  ./generate_opc_configs.py --device 1 --link $i opc
done

for i in `seq 0 7`
do
  ./generate_opc_configs.py --device 0 --link $i unit
  ./generate_opc_configs.py --device 1 --link $i unit
done
"""
    )

parser.add_argument('config_file', type=str, help="one of config files: opc or com")

parser.add_argument('--etc-dir',   type=str, default='/opt/atlas/etc/', help="the root etc directory for these configs (/opt/atlas/etc/)")

parser.add_argument('--subsystem', type=str, default='flx03', help="subsystem name (flx03)")
parser.add_argument('--device',    type=int, default=0,       help="FLX device to use (0)")
parser.add_argument('--link',      type=int, default=0,       help="optical link to use (1)")
parser.add_argument('--hostname',  type=str, default='pcatlitkflx03', help="the FLX server hostname (pcatlitkflx03)")

parser.add_argument('-d', '--debug',    action='store_true', help="DEBUG level of logging")

args = parser.parse_args()

if args.debug:
    logging.basicConfig(level=logging.DEBUG)

else:
    logging.basicConfig(level=logging.INFO)


subsystem_name = args.subsystem
device_num = args.device
link_num   = args.link
flx_hostname = args.hostname

ic_elink_rx = 29 + 0x40*link_num + 0x800*device_num
ic_elink_tx = 21 + 0x40*link_num + 0x800*device_num

ec_elink_rx = 28 + 0x40*link_num + 0x800*device_num
ec_elink_tx = 20 + 0x40*link_num + 0x800*device_num

flx_port_rx = 12351 if device_num > 0 else 12350
flx_port_tx = 12341 if device_num > 0 else 12340

opc_config_file  = f'{args.etc_dir}/amac_opc_configs/' + subsystem_name + f'/{device_num}/config_link{link_num:02}.xml'

opc_systemd_file = f'/etc/systemd/system/itk_monitor_amacs_{subsystem_name}_{device_num}_link{link_num:02}.service'

ec_comfile_path  = f'{args.etc_dir}/amac_opc_com_configs/' + subsystem_name + f'/{device_num}/cfg_endeavour_link{link_num:02}.json'
ec_lockfile_path = f'{args.etc_dir}/amac_lockfiles/'       + subsystem_name + f'/{device_num}/lock_link{link_num:02}'

# the calibrations directory is not generated currently
# only the path is passed to the OPC config
ec_calibdir_path = f'{args.etc_dir}/amac_calibrations/'    + subsystem_name + f'/{device_num}/link{link_num:02}/'


opc_config = f'''<?xml version="1.0" encoding="UTF-8"?>
<configuration xmlns="http://cern.ch/quasar/Configuration" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://cern.ch/quasar/Configuration ../Configuration/Configuration.xsd ">

<SubSystem name="{subsystem_name}">

       <FibreA name="dev{device_num}_link{link_num:02}" RefreshPeriod_msec="7000" InfluxHostname="atlflxitkstrip02">

                       <LpGBT
                              hostname="{flx_hostname}"
                              portRX="{flx_port_rx}"
                              portTX="{flx_port_tx}"
                              elinkRX="{ic_elink_rx}"
                              elinkTX="{ic_elink_tx}"
                              name="lpGBT"
                              Path="ic_over_netio"
                              I2C="113"

                              ConfigFile="default"
                              SecLpGBTPort="-1"
                              SecLpGBTConfigFile="default"
                              SecLpGBTI2C="112"
                              >
                       </LpGBT>

               <Bus name="bus0" BusIdentifier="8" UploadToInflux="true" ComBackend="flx"
                       BusEnabledRegisters="temperaturePTAT temperaturePB temperatureCTAT CAL getCur1V getCur10V HVret getHVret getHVcur DCDCen DCDCenC temperatureX NTCX CUR1V CUR10V"
                       Path="module"
                       ComFile="{ec_comfile_path}"
                       CalibrationsDir="{ec_calibdir_path}"
                       AutoCalibrate="false" AutoLoadCalibration="true">

                       <AMAC name="amac00" AMACID= "0" BONDID= "0" FUSEID= "0" AMACVersion="v2" MonitorDCDC="true" CircuitType="stave" Enabled="true"></AMAC>
                       <AMAC name="amac01" AMACID= "1" BONDID= "1" FUSEID= "1" AMACVersion="v2" MonitorDCDC="true" CircuitType="stave" Enabled="true"></AMAC>
                       <AMAC name="amac02" AMACID= "2" BONDID= "2" FUSEID= "2" AMACVersion="v2" MonitorDCDC="true" CircuitType="stave" Enabled="true"></AMAC>
                       <AMAC name="amac03" AMACID= "3" BONDID= "3" FUSEID= "3" AMACVersion="v2" MonitorDCDC="true" CircuitType="stave" Enabled="true"></AMAC>
                       <AMAC name="amac04" AMACID= "4" BONDID= "4" FUSEID= "4" AMACVersion="v2" MonitorDCDC="true" CircuitType="stave" Enabled="true"></AMAC>
                       <AMAC name="amac05" AMACID= "5" BONDID= "5" FUSEID= "5" AMACVersion="v2" MonitorDCDC="true" CircuitType="stave" Enabled="true"></AMAC>
                       <AMAC name="amac06" AMACID= "6" BONDID= "6" FUSEID= "6" AMACVersion="v2" MonitorDCDC="true" CircuitType="stave" Enabled="true"></AMAC>
                       <AMAC name="amac07" AMACID= "7" BONDID= "7" FUSEID= "7" AMACVersion="v2" MonitorDCDC="true" CircuitType="stave" Enabled="true"></AMAC>
                       <AMAC name="amac08" AMACID= "8" BONDID= "8" FUSEID= "8" AMACVersion="v2" MonitorDCDC="true" CircuitType="stave" Enabled="true"></AMAC>
                       <AMAC name="amac09" AMACID= "9" BONDID= "9" FUSEID= "9" AMACVersion="v2" MonitorDCDC="true" CircuitType="stave" Enabled="true"></AMAC>
                       <AMAC name="amac10" AMACID="10" BONDID="10" FUSEID="10" AMACVersion="v2" MonitorDCDC="true" CircuitType="stave" Enabled="true"></AMAC>
                       <AMAC name="amac11" AMACID="11" BONDID="11" FUSEID="11" AMACVersion="v2" MonitorDCDC="true" CircuitType="stave" Enabled="true"></AMAC>
                       <AMAC name="amac12" AMACID="12" BONDID="12" FUSEID="12" AMACVersion="v2" MonitorDCDC="true" CircuitType="stave" Enabled="true"></AMAC>
                       <AMAC name="amac13" AMACID="13" BONDID="13" FUSEID="13" AMACVersion="v2" MonitorDCDC="true" CircuitType="stave" Enabled="true"></AMAC>
               </Bus>
       </FibreA>

</SubSystem>

</configuration>
'''

ec_comfile = f'''
{{
  "ec_elinks": {{
    "tx_elink_n": {ec_elink_tx},
    "rx_elink_n": {ec_elink_rx},
    "lock_filename": "{ec_lockfile_path}"
  }},

  "icnetio": {{
    "flxHost": "{flx_hostname}",
    "portFromHost": {flx_port_tx},
    "portToHost":   {flx_port_rx},
    "tx_elink_n": {ic_elink_tx},
    "rx_elink_n": {ic_elink_rx}
  }}
}}
'''

systemd_unit = f'''
[Unit]
Description=ITk Strips monitor AMACs on {subsystem_name} {device_num} link{link_num:02}

[Service]
User=itkfelixstrips
Group=itkfelixstrips
WorkingDirectory=/home/itkfelixstrips/software/opcua_quasar/temp2/opc-server/build/bin/
ExecStart=/home/itkfelixstrips/software/opcua_quasar/temp2/opc-server/build/bin/OpcUaServer --config_file {opc_config_file} --opcua_backend_config {4850 + device_num*10 + link_num + (50 if subsystem_name == 'flx03' else 0)}
Slice=itkstrips-dcs.slice

Restart=on-failure
RestartSec=20

StartLimitInterval=300
StartLimitBurst=5

[Install]
WantedBy=multi-user.target
'''

if args.config_file == 'opc':
    print(opc_config_file)
    with open(opc_config_file, 'w') as f:
        f.write(opc_config)

elif args.config_file == 'unit':
    print(opc_systemd_file)
    with open(opc_systemd_file, 'w') as f:
        f.write(systemd_unit)

elif args.config_file == 'com':
    print(ec_comfile_path)
    with open(ec_comfile_path, 'w') as f:
        f.write(ec_comfile)

elif args.config_file == 'lock':
    print(ec_lockfile_path)
    with open(ec_lockfile_path, 'w') as f:
        f.write('\n')

else:
    print(f'unknown config file type: {args.config_file}')

