OPC server for Autonomous Monitoring and Control (AMAC) chips in ITk Stirps.
Main development repository is in [SR1 gitlab group](https://gitlab.cern.ch/itk-strips-at-sr1/opc_server_amacv2).

# Building

Install Quasar OPC framework dependencies.
They are listed in the [quasar docs](https://quasar.docs.cern.ch/quasar.html),
e.g. [Alma 9](https://quasar.docs.cern.ch/quasar.html#quick-setup-hints-for-almalinux-9-the-most-standard-way):

```
sudo dnf install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm

sudo dnf install git patch python3-colorama python3-jinja2 python3-lxml cmake3 gcc-c++ boost-devel graphviz ninja-build xsd xerces-c-devel libxml2-devel openssl-devel doxygen astyle
```

The backend AMAC code is included in the OPC server as QuasarModuleITkStripsDCS module.
It is really an [external cmake project](https://gitlab.cern.ch/otoldaie/dcs-tests-reborn) that
serves as a preliminary umbrella repository for AMAC code, connectivity backends, etc.
Hence, clone the server source code recursively, and build with the usual quasar build command:

```
git clone --recursive ssh://git@gitlab.cern.ch:7999/itk-strips-at-sr1/opc_server_amacv2.git
# or
# git clone --recursive https://gitlab.cern.ch/itk-strips-at-sr1/opc_server_amacv2.git
./quasar.py build Debug
```

Notice, by default `ProjectSettings.cmake` define only ITSDAQ' endeavourTest backend to be built:

```
# ProjectSettings.cmake
  set(BUILD_endeavourTest TRUE)
  set(BUILD_FELIXCLIENT FALSE)
```

You can change one of options to false, if needed.
If FelixClient backend is built, the server will try to use it first,
and then it will move on to `ComBackend="endeavourTest"` for ITSDAQ.


## More dependencies

These were needed at some point.

For Quasar:

```
pip install --user colorama
# also sudo yum install astyle ?
```

For felixclient: somehow the CVMFS setup misses zeromq?



# Additional hacks and patches that are not in git


## OPC server port number instead of --server_config

If you look at how the OPC server works in SR1, there is an ad-hoc hack to pass the port number as one of the command line options:

```
$ systemctl status itk_monit*
● itk_monitor_amacs_flx03_0_link01.service - ITk Strips monitor AMACs on flx03 0 link01
   Loaded: loaded (/etc/systemd/system/itk_monitor_amacs_flx03_0_link01.service; disabled; vendor preset: disabled)
...

● itk_monitor_amacs_flx03_0_link03.service - ITk Strips monitor AMACs on flx03 0 link03
   Loaded: loaded (/etc/systemd/system/itk_monitor_amacs_flx03_0_link03.service; disabled; vendor preset: disabled)
...

$ cat /etc/systemd/system/itk_monitor_amacs_flx03_0_link03.service
...
ExecStart=/home/itkfelixstrips/software/opcua_quasar/temp2/opc-server/build/bin/OpcUaServer --config_file /opt/atlas/etc//amac_opc_configs/flx03/0/config_link03.xml --opcua_backend_config 4903
...
```

The option `--opcua_backend_config` is supposed to pass a file ServerConfig.xml.
But usually our OPC backend library open6 is compiled without the support for this config file.
(OPC server prints a warning about it at the startup.)
To be able to change the OPC server port, there is a hack to pass an integer in that option.
The option is processed in:

```
build/open62541-compat/src/uaserver.cpp

void UaServer::setServerConfig(
        const UaString& configurationFile,
        const UaString& applicationPath)
{
#ifndef HAS_SERVERCONFIG_LOADER
    LOG(Log::INF) << "Note: you built open62541-compat without configuration loading (option SERVERCONFIG_LOADER). So loading of ServerConfig.xml is not supported. Assuming hardcoded server settings (endpoint's port, etc.)";
    //! With open62541 1.0, it is the UA_Server that holds the config.

    // hack different ports
    LOG(Log::INF) << "a hack to use the server config path to pass the port number";

    auto cfg_option = configurationFile.toUtf8();
    if (cfg_option.empty()) return; // use the hardcoded defaults
    // else -- try to use it as a port number

    try {
      unsigned int endpointUrlPort = std::stoul(cfg_option);
      LOG(Log::INF) << "From your [" << configurationFile.toUtf8() << "] loaded endpoint port number: " << endpointUrlPort;
      m_endpointPortNumber = endpointUrlPort;
    }
    catch(...) {
      LOG(Log::INF) << "From your [" << configurationFile.toUtf8() << "] could NOT load endpoint port number";
      throw;
    }

#else // HAS_SERVERCONFIG_LOADER is defined, means the user wants the option
...
}
```


## Local `astyle`

Our CentOS 7 machines have everything outdated, including `astyle`, which is used in Quasar.
So, a recent version of astyle is built in the home directory of atl02,
and the OPC server has to point to it:

```
FrameworkInternals/transformDesign.py

     try:
-        completed_indenter_process = subprocess.run(['astyle'], input=unindented_content,
+        #completed_indenter_process = subprocess.run(['astyle'], input=unindented_content,
+        completed_indenter_process = subprocess.run(['/home/itkfelixstrips/software/opcua_quasar/astyle/as-gcc-exe/astyle'], input=unindented_conte
                                                     stdout=subprocess.PIPE, check=True)
```


